package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testF_Positive() throws Exception {
	int a = 10;
	int b = 15;
	
	int c = new MinMax().f(a,b);

//        final String str = new MinMax().f();
        assertTrue("True if b is bigger than a",c > a);
        }


    @Test
    public void testF_Negative() throws Exception {
        int a = 10;
        int b = 5;

        int c = new MinMax().f(a,b);

        assertTrue("True if b is smaller than a",b < c);
       }

}


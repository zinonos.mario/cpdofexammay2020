package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {
    @Test
    public void testDesc() throws Exception {
	final String str = new AboutCPDOF().desc();
	assertTrue("DescPage",str.toString().contains("CP-DOF certification program"));
	}
}
